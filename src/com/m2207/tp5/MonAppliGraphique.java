package com.m2207.tp5;

import javax.swing.*;
import java.awt.*;

public class MonAppliGraphique extends JFrame
{
    //Ex 2
    //public static JButton BtnHello;

    //Ex 2.3
    public static final int NbrButton = 5;
    private static JButton Buttons[] = new JButton[NbrButton];

    //Ex 2.4
    private static JLabel MonLabel;
    public static JTextField MonTextField;

    public  MonAppliGraphique ()
    {
        super();
        setTitle("Mapremière application");
        setSize(400,200);
        setLocation(20,20);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        System.out.println("La fenêtre est créée !");

        //Ex 2
        //BtnHello = new JButton("Hello world");
        Container _panneau = getContentPane();

        //Ex 2.2
        //Qu’est-ce qui a changé ?
        //Le button ne prend plus tou l'espace disponible lorsque nous modifion
        // la taille de la fenêtre.
        //_panneau.setLayout(new FlowLayout());

        //Ex 3
        //_panneau.setLayout(new BorderLayout());

        //Ex 3.2
        _panneau.setLayout(new GridLayout(3,2));

        //Ex 2
        //_panneau.add(BtnHello);

        //Ex 2.3
        for (int i = 0; i < Buttons.length; i++)
        {
            //Ex 2.3
            Buttons[i] = new JButton("Bouton " + i);
            //Ex 2.3 & 3.2
            _panneau.add(Buttons[i]);

            //Ex 3
            /*switch (i)
            {
                case 1:
                    _panneau.add(Buttons[i], BorderLayout.NORTH);
                    break;

                case 2:
                    _panneau.add(Buttons[i], BorderLayout.EAST);
                    break;

                case 3:
                    _panneau.add(Buttons[i], BorderLayout.SOUTH);
                    break;

                case 4:
                    _panneau.add(Buttons[i], BorderLayout.WEST);
                    break;

                default:
                    _panneau.add(Buttons[i]);
                    break;
            }*/
        }

        //Ex 2.4
        //MonLabel = new JLabel("Je suis un JLabel");
        //_panneau.add(MonLabel);
        //MonTextField = new JTextField("Je suis un JTextField");
        //_panneau.add(MonTextField);

        setVisible(true);
    }
}
