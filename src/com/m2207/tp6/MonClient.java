package com.m2207.tp6;

import java.io.PrintWriter;
import java.net.Socket;

public class MonClient
{
    public static void main(String[] args)
    {
        //EX 4.1
        Socket monSocket;
        try
        {
            //Ex 4.1
            monSocket = new Socket("localhost",8888);
            System.out.printf("Client : %s\n", monSocket);

            //Ex 4.2
            //Quel est l’intérêt de l’appel à la méthode flush() ?
            // Elle sert a rincé le flux. En rinçant le flux,
            // cela signifie supprimer le flux de tout élément qui peut être ou non à l'intérieur du flux.
            PrintWriter monPrintWriter = new PrintWriter(monSocket.getOutputStream());
            System.out.println("Envoi du message : Hello World");
            monPrintWriter.println("Hello World");
            monPrintWriter.flush();

            monSocket.close();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }
}
