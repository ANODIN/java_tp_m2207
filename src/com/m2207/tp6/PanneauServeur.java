package com.m2207.tp6;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.net.Socket;

public class PanneauServeur extends JFrame implements ActionListener
{
    protected JTextArea TextArea;
    protected JButton ExitButton;

    protected ServerSocket monServerSocket;
    protected Socket monSocketClient;
    protected BufferedReader monBufferedReader;;

    public static void main(String[] args)
    {
        //Ex 5.1
        new PanneauServeur();
    }

    //EX 5.1
    public PanneauServeur ()
    {
        super();
        setTitle("Serveur - Panneau d'affichage");
        setSize(400,300);
        setLocation(20,20);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        Container _panneau = getContentPane();
        _panneau.setLayout(new BorderLayout());
        TextArea = new JTextArea();
        ExitButton = new JButton("Exit");
        _panneau.add(TextArea, BorderLayout.NORTH);
        _panneau.add(ExitButton, BorderLayout.SOUTH);

        ExitButton.addActionListener(this);

        TextArea.append("Le panneau est actif\n");

        setVisible(true);

        StartServer();
        ecouter();
    }

    //Ex 5.2
    private void StartServer ()
    {
        try
        {
            monServerSocket = new ServerSocket(8888);
            TextArea.append("Serveur démarré\n");
        }
        catch (Exception e)
        {
            TextArea.append("Erreur dans le traitement de la connexion\n");
        }
    }

    //Ex 5.3
    private void ecouter ()
    {
        try
        {
            TextArea.append("Serveur en attente de connexion...\n");
            monSocketClient = monServerSocket.accept();
            TextArea.append("Client connecté\n");

            monBufferedReader = new BufferedReader(new InputStreamReader(monSocketClient.getInputStream()));

            String line;
            while ((line = monBufferedReader.readLine()) != null)
            {
                TextArea.append("Message : " + line + "\n");
            }
        }
        catch (Exception e)
        {
            TextArea.append("Erreur de création ServerSocket\n");
        }
    }

    public void actionPerformed (ActionEvent actionEvent)
    {
        try
        {
            monSocketClient.close();
            monServerSocket.close();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        System.exit(-1);
    }
}
