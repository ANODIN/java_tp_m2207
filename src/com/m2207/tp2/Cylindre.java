package com.m2207.tp2;

public class Cylindre extends Cercle
{
	private double Hauteur;
	
	public double getHauteur () { return Hauteur; }
	public void setHauteur (double _h) { Hauteur = _h; }
	
	public Cylindre ()
	{
		super();
		Hauteur = 1.0;
	}
	
	public Cylindre (double _h, double _r, String _couleur, boolean _coloriage)
	{
		super(_r, _couleur, _coloriage);
		Hauteur = _h;
	}
	
	public String seDecrire ()
	{
		return "Un Cylindre d'hauteur "+ Hauteur +" composer d'" + super.seDecrire();
	}
	
	public double CalculerVolume ()
	{
		return CalculerAire() * Hauteur;
	}
}
