package com.m2207.tp1;

/*
 * Cette classe permet a nos clients d'avoir plusieurs comptes
 */
public class ClientMultiComptes
{
	private String Nom, Prenom;
	private Compte[] Tabcomptes = new Compte[10];
	
	private int NbCompte;
	
	public double getSolde ()
	{
		double _result = 0;
		for(Compte _item : Tabcomptes)
			if(_item != null) _result += _item.getSolde();
		
		return _result;
	}
	
	public ClientMultiComptes (String _nom, String _prenom, Compte _compte)
	{
		Nom = _nom;
		Prenom = _prenom;
		
		Tabcomptes[0] = _compte;
	}
	
	/*
	 * Permet d'ajouter au client un nouveau compte pass� en argument.
	 */
	public void AjouterCompte (Compte _c)
	{
		for(int i = 0; i < 10; i++)
		{
			if(Tabcomptes[i] == null)
			{
				Tabcomptes[i] = _c;
				break;
			}
		}
	}
	
	public void AfficherEtatClient ()
	{
		System.out.printf("Client : %s %s\n",
				Prenom,
				Nom);
		for(int i = 0; i < 10; i++)
			if(Tabcomptes[i] != null) System.out.printf("Compte : %d - Solde : %.1f\n",
					Tabcomptes[i].getNumero(),
					Tabcomptes[i].getSolde());
		System.out.printf("Solde Client : %.1f\n", getSolde());
	}
}
