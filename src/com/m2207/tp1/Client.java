package com.m2207.tp1;

public class Client
{
	private String FirstName;
	private String LastName;
	
	private Compte CompteCourant;
	
	public String getFirstName() { return FirstName; }
	
	public String getLastName () { return LastName; }
	
	public double getSolde () { return CompteCourant.getSolde(); }
	
	public void AfficherSolde ()
	{
		System.out.printf("Solde : %.1f\n", getSolde());
	}
	
	public Client (String _firstName, String _lastName, Compte _compte)
	{
		FirstName = _firstName;
		LastName = _lastName;
		
		CompteCourant = _compte;
	}
	
}
