package com.m2207.tp1;

public class TestClient
{

	public static void main(String[] args)
	{
		//Test du client pour ex 3.2 avec atribue compte au constructeur
		Client _newClient = new Client("Jean", "Doe", new Compte(1));
		
		//Test de la nouvelle fonction de ex 3.3
		_newClient.AfficherSolde();
		
		System.out.printf("Prenom : %s, Nom fammille : %s\n", _newClient.getFirstName(), _newClient.getLastName());
		
		//Test de la class ClientMultiComptes
		Compte _clientPAccountOne = new Compte(5);
		_clientPAccountOne.Depot(1000);
		ClientMultiComptes _clientP = new ClientMultiComptes("Paul","Delapoire", _clientPAccountOne);
		
		Compte _clientPAccountTwo = new Compte(6);
		_clientPAccountTwo.Depot(50);
		_clientP.AjouterCompte(_clientPAccountTwo);
		
		System.out.printf("Somme des soldes : %.1f\n", _clientP.getSolde());
	}

}
