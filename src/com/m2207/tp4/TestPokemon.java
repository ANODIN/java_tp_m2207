package com.m2207.tp4;

public class TestPokemon
{
    public static void main(String[] args)
    {
        exerciceSpace("1.3");
        Pokemon _etre = new Pokemon("MPokAura");
        _etre.sePresenter();

        exerciceSpace("1.5");
        _etre.manger();
        _etre.manger();
        _etre.manger();

        exerciceSpace("1.6");
        _etre.vivre();
        _etre.vivre();
        _etre.vivre();
        _etre.sePresenter();

        exerciceSpace("1.8");
        Pokemon _pokba = new Pokemon("Pokba");
        _pokba.sePresenter();
        _pokba.nbrCycle();
    }

    private static void exerciceSpace (String _number)
    {
        System.out.printf("\nexercice %s ----------------------------------------\n\n", _number);
    }
}
