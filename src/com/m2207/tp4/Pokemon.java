package com.m2207.tp4;

import java.lang.Object;
import java.util.Random;

public class Pokemon
{
    private int Energie, MaxEnergie, Puissance;
    private String Nom;

    public String getNom() {
        return Nom;
    }

    public int getEnergie() {
        return Energie;
    }

    public int getPuissance() {
        return Puissance;
    }

    public Pokemon (String _nom)
    {
        Nom = _nom;
        MaxEnergie = getIntRand(50, 90);
        Energie = getIntRand(30, MaxEnergie);

        Puissance = getIntRand(3, 10);
    }

    public void sePresenter ()
    {
        System.out.printf("Je suis %s, j'ai %d points d'energie (%d max) et une puissance de %d.\n",
                Nom,
                Energie,
                MaxEnergie,
                Puissance);
    }

    public void manger ()
    {
        int _nQty = getIntRand(10,30);

        if(Energie >= MaxEnergie)
        {
            System.out.printf("Je suis %s, je jette %d de nourriture, j'ai %d points d'energie (%d max).\n",
                    Nom,
                    _nQty,
                    Energie,
                    MaxEnergie);
        }
        else
        {
            int _newQty = Energie + _nQty;

            if(_newQty > MaxEnergie)
            {
                int _reste = (_newQty - MaxEnergie);

                Energie += (_nQty - _reste);

                System.out.printf("Je suis %s, je mange %d et je jette %d de nourriture, j'ai %d points d'energie (%d max).\n",
                        Nom,
                        (_nQty - _reste),
                        _reste,
                        Energie,
                        MaxEnergie);
            }
            else
                Energie += _nQty;
        }
    }

    public void vivre ()
    {
        int _value = getIntRand(20,40);

        if((Energie - _value) <= 0)
            Energie = 0;
        else
            Energie -= _value;
    }

    public boolean isAlive ()
    {
        return (Energie <= 0) ? false : true;
    }

    public void nbrCycle ()
    {
        int _cycle = 0;
        while (isAlive())
        {
            manger();
            vivre();
            _cycle++;
        }
        System.out.printf("%s a vécu %d cycles !\n",
                Nom,
                _cycle);
    }

    public void perdreEnergie (int _perte)
    {
        if(isFurie()) Energie -= (int)(_perte * 1.5);
        else Energie -= _perte;
        if(Energie < 0) Energie = 0;
    }

    public void attaquer (Pokemon _adversaire)
    {
        int _puissance = Puissance;

        Puissance -= getIntRand(0,1);
        if(Puissance <= 0) Puissance = 1;

        if(isFurie()) _puissance = Puissance * 2;

        _adversaire.perdreEnergie(_puissance);
    }

    public boolean isFurie ()
    {
        return (((float)Energie/(float)MaxEnergie) < 0.25f) ? true : false;
    }

    public int getIntRand (int _min, int _max)
    {
        return _min  + (int) (Math.random() * ((_max - _min) + 1));
    }
}
