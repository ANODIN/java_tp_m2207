package com.m2207.tp3;

public class Sherif extends Cowboy
{
    private int NbrBrigandArreter;

    public Sherif (String _nom)
    {
        super(_nom);
        NbrBrigandArreter = 0;
    }

    public String QuelEstTonNom()
    {
        return "Shérif " + Nom;
    }

    public void SePresenter()
    {
        super.SePresenter();
        Parler("J'ai déjà arrêté " + NbrBrigandArreter +" brigand(s)");
    }

    public void Coffrer (Brigand _b)
    {
        NbrBrigandArreter ++;
        Parler("Au nom de la loi, je vous arrête, " + _b.Nom);
        _b.Emprisonner(this);
    }
}
