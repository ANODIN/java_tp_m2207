package com.m2207.tp3;

public class Histoire
{
    public static void main(String[] args)
    {
        //EX
        Humain _bobHumain = new Humain("bob");
        _bobHumain.SePresenter ();
        _bobHumain.Boire();

        //EX 2.4
        Dame _rachelDame = new Dame ("Rachel");
        _rachelDame.SePresenter();
        _rachelDame.PriseEnOtage ();
        _rachelDame.EstLiberee();

        //Ex 3.2
        Brigand _guntherBrigand = new Brigand("Gunther");
        _guntherBrigand.SePresenter();

        //EX 4.3
        Cowboy _simonCowboy = new Cowboy("Simon");
        _simonCowboy.SePresenter();

        //EX 7.2
        _guntherBrigand.Enleve(_rachelDame);
        _rachelDame.SePresenter();
        _guntherBrigand.SePresenter();

        //EX 7.5
        _simonCowboy.Tire(_guntherBrigand);
        _simonCowboy.Libere(_rachelDame);
        _rachelDame.SePresenter();

        //EX 8.1
        Sherif _sherifMarshall = new Sherif("Marshall");
        _sherifMarshall.SePresenter();

        //EX 8.4
        _sherifMarshall.Coffrer(_guntherBrigand);
        _sherifMarshall.SePresenter();

        //EX 9
        //Quelles sont les méthodes que l'on peut appeler ?
        //Nous pouvons apeler tous les methodes de la class Cowboy
        Cowboy _cowboyClint = new Sherif("Clint");
        _cowboyClint.SePresenter();
        //Peut-on demander à ce cowboy de coffrer un brigand ? Pourquoi ?
        //Oui car c'est une class sherif instancier dans une class cowboy
        //ainsi il est juste limiter au fonction du cowboy. Par consequence pour pouvoir
        //coffrer un brigand nous devons faire un cast comme ce qui suit
        Brigand _johnBrigand = new Brigand("John");
        ((Sherif)_cowboyClint).Coffrer(_johnBrigand);

        //EX 10
        Prison _prison = new Prison("Fort Alamo");
        _prison.CompterLesPrisonniers();
        _prison.MettreEnCellule(_guntherBrigand);
        _prison.CompterLesPrisonniers();
        _prison.SortirDeCellule(_guntherBrigand);
        _prison.CompterLesPrisonniers();

        //EX 11
        Cachot _cachot = new Cachot("Open Cachot");
        _cachot.CompterLesPrisonniers();
        _cachot.MettreEnCellule((Humain)_rachelDame);
        _cachot.CompterLesPrisonniers();
        _cachot.SortirDeCellule((Humain)_rachelDame);
        _cachot.CompterLesPrisonniers();
    }
}
